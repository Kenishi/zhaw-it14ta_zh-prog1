import java.util.HashMap;
import java.util.Set;
/**
 * Diese Klasse modelliert R�ume in der Welt von Zuul.
 * 
 * Ein "Raum" repr�sentiert einen Ort in der virtuellen Landschaft des
 * Spiels. Ein Raum ist mit anderen R�umen �ber Ausg�nge verbunden.
 * M�gliche Ausg�nge liegen im Norden, Osten, S�den und Westen.
 * F�r jede Richtung h�lt ein Raum eine Referenz auf den 
 * benachbarten Raum.
 * 
 * @author  Michael K�lling und David J. Barnes
 * @version 31.07.2011
 */
public class Raum 
{
    private String beschreibung;
    private HashMap<String, Raum> ausgaenge;
    

    /**
     * Erzeuge einen Raum mit einer Beschreibung. Ein Raum
     * hat anfangs keine Ausg�nge.
     * @param beschreibung enth�lt eine Beschreibung in der Form
     *        "in einer K�che" oder "auf einem Sportplatz".
     */
    public Raum(String beschreibung) 
    {
        this.beschreibung = beschreibung;
        ausgaenge = new HashMap<String, Raum>();
    }

    /**
     * Definiere die Ausg�nge dieses Raums. Jede Richtung
     * f�hrt entweder in einen anderen Raum oder ist 'null'
     * (kein Ausgang).
     * @param norden Der Nordausgang.
     * @param osten Der Ostausgang.
     * @param sueden Der S�dausgang.
     * @param westen Der Westausgang.
     */
    public void setzeAusgaenge(String richtung, Raum raum) 
    {
        ausgaenge.put(richtung, raum);
    }
    
    public Raum gibAusgang(String richtung)
    {
        return ausgaenge.get(richtung);
    }

    private String gibAusgaengeAlsString()
    {
        String ergebnis = "Ausg�nge:";
        Set<String> keys = ausgaenge.keySet();
        
        for(String ausgang : keys)
        {
            ergebnis += " " + ausgang;
        }
        
        return ergebnis;
    }
    
    /**
     * @return die Beschreibung dieses Raums.
     */
    public String gibBeschreibung()
    {
        return beschreibung;
    }
    
    public String gibLangeBeschreibung()
    {
        return "Sie sind " + gibBeschreibung() + ".\n" + gibAusgaengeAlsString();
    }
}
