import org.junit.*;

public class PruefungsverwaltungTest {
	Pruefungsverwaltung pruefungsverwaltung;
	
	@Before
	public void setup()
	{
		pruefungsverwaltung = new Pruefungsverwaltung();
	}
	
	@Test
	public void roundDown()
	{
		Assert.assertEquals(pruefungsverwaltung.rundeAufHalbeNote(1.6),1.5,0.1);
	}
	
	@Test
	public void roundUp()
	{
		Assert.assertEquals(pruefungsverwaltung.rundeAufHalbeNote(1.4),1.5,0.1);
	}
	
	@Test
	public void doNotRound()
	{
		Assert.assertEquals(pruefungsverwaltung.rundeAufHalbeNote(1.0),1.0,0.1);
	}
	
	@Test
	public void roundUpToFull()
	{
		Assert.assertEquals(pruefungsverwaltung.rundeAufHalbeNote(1.9),2.0,0.1);	
	}
	
	@Test
	public void roundDownToFull()
	{
		Assert.assertEquals(pruefungsverwaltung.rundeAufHalbeNote(1.1),1.0,0.1);	
	}
	
	@Test
	public void checkOutputUnderFour()
	{
		Assert.assertEquals(pruefungsverwaltung.generiereText("Student", 3.5),"Student, Sie haben an der Pruefung eine 3.5 (drei punkt fuenf) erzielt und sind somit durchgefallen!");
	}
	
	@Test
	public void checkOutputOverFour()
	{
		Assert.assertEquals(pruefungsverwaltung.generiereText("Student", 4.5),"Herzliche Gratulation Student! Sie haben an der Pruefung eine 4.5 (vier punkt fuenf) erzielt und somit bestanden!");
	}
	
	@Test
	public void checkInvalidGrade()
	{
		Assert.assertEquals(pruefungsverwaltung.generiereText("Student", 4.2),"Herzliche Gratulation Student! Sie haben an der Pruefung eine 4.2 (null) erzielt und somit bestanden!");
	}
	
	@Test
	public void checkInvalidStudent()
	{
		Assert.assertEquals(pruefungsverwaltung.generiereText(null, 4.2),"Herzliche Gratulation null! Sie haben an der Pruefung eine 4.2 (null) erzielt und somit bestanden!");
	}
}
