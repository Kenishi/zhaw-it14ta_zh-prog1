import org.junit.*; 

public class ZufaelligeNotengebungTest {
	ZufaelligeNotengebung zufaelligeNotengebung;
	
	@Before
	public void setup()
	{
		zufaelligeNotengebung = new ZufaelligeNotengebung();
	}
	
	@Test
	public void coversAllGrades()
	{
		boolean failed = false;
		for(int i=0; i<10000000 && !failed; i++)
		{
			double note = zufaelligeNotengebung.generiereZufaelligePruefungsnote();
			
			if (note < 1.0 || note > 6.0) {
				failed = true;
			}	
		}
		
		Assert.assertFalse(failed);
	}
}
