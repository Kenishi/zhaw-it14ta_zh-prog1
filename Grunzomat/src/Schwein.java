
public class Schwein extends Nutztier 
{
	private int grunzLautstaerke;
	
	public Schwein(int alter, int gewicht, int grunzLautstaerke)
	{
		super(alter, gewicht);
		this.grunzLautstaerke = grunzLautstaerke;
	}
	
	public void grunzen()
	{
		System.out.print("Gru");
		for(int i = 0; i<grunzLautstaerke; i++)
		{
			System.out.println("u");
		}
		
		System.out.println("nz");
	}
}
