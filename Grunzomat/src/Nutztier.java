
public class Nutztier {
	public int alter;
	public int gewicht;
	
	public Nutztier(int alter, int gewicht)
	{
		this.alter = alter;
		this.gewicht = gewicht;
	}
	
	public int gibAlter()
	{
		return alter;
	}
	
	public int gibGewicht()
	{
		return gewicht;
	}
}
