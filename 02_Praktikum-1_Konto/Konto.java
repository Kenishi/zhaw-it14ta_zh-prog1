
/**
 * Write a description of class Konto here.
 * 
 * @version (1)
 */
public class Konto
{
   private String kontoinhaber;
   private int kontostand;
   private int zinssatz;
 
   /** Initialisiert ein neues Konto mit einem Kontoinhaber
    * 
    *  @param (String kontoInhaber)
    */
   public Konto(String kontoInhaber) {
       kontoinhaber = kontoInhaber;
       zinssatz = 2;
       kontostand = 0;
   }
    
   /** Initialisiert ein neues Konto mit Kontoinhaber und Zinssatz
    * 
    *  @param (String kontoInhaber)
    *  @parmm (int zins)
    */
   public Konto(String kontoInhaber, int zins) {
       kontoinhaber = kontoInhaber;
       zinssatz = zins;
       kontostand = 0;
   }
   
   /** Gibt den aktuellen Kontostand zurück
    * 
    */
   public int getKontostand() {
       return kontostand;
   }
   
   /** Setzt einen neuen Kontostand
    *  
    *  @param (int neuerKontostand)
    */
   public void setKontostand(int newKontostand) {
       kontostand = newKontostand;
   }
   
   /** Fuegt den Betrag zum aktuellen Kontostand hinzu
    * 
    *  @param (int betrag)
    */
   public void addMoney(int money) {
       kontostand = kontostand + money;
   }
   
   /** Hebt einen Betrag vom Kontostand ab
    * 
    *  @param (int betrag)
    */
   public void retrieveMoney(int money) {
       kontostand = kontostand - money;
   }
   
   /** Berechnet den Zins des Kontostandes in Abhängigkeit des Zinssatzes
    * 
    */
   public int gibtZinsVonKontostand() {
       return kontostand / 100 * zinssatz;
   }
   
   /** Zeigt diverse Kontoinformationen in der Konsole an
    * 
    */
   public void zeigeKontoInformationen() {
       System.out.println("Informationen zum Konto");
       System.out.println("Kontoinhaber: " + kontoinhaber);
       System.out.println("Kontostand: " + kontostand);
       System.out.println("Zinssatz: " + zinssatz + "%");
       
   }
    
}
