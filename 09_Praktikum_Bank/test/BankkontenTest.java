import org.junit.Test;
import org.junit.Assert;

public class BankkontenTest 
{	
	@Test
	public void isAbleToCreateKontoWithInhaber()
	{
		Bankkonto bankkonto = new Bankkonto("Thomas Seibert",0);
		
		Assert.assertEquals("Inhaber: Thomas Seibert, Kontostand: 0.0", bankkonto.toString());
	}
	
	@Test
	public void canNotHaveMoreThanHundredThousand()
	{
		Bankkonto bankkonto = new Bankkonto("Thomas Seibert", 100001);
		
		Assert.assertEquals(10000000, bankkonto.getKontostand());
	}
	
	@Test
	public void canNotWithdrawBelowZero()
	{
		Bankkonto bankkonto = new Bankkonto("Thomas Seibert", 0);
		bankkonto.geldAbheben(10);
		
		Assert.assertEquals(0, bankkonto.getKontostand());
	}
	
	@Test
	public void canNotAddMoreThanHundredThousand()
	{
		Bankkonto bankkonto = new Bankkonto("Thomas Seibert", 0);
		bankkonto.geldEinzahlen(100001);
		
		Assert.assertEquals(10000000, bankkonto.getKontostand());
	}
	
	@Test
	public void salaerLimitNotNegative()
	{
		Salaerkonto salaerkonto = new Salaerkonto("Thomas Seibert", 0, -10000);
		
		Assert.assertEquals(0, salaerkonto.getUeberzugslimite());
	}
	
	@Test
	public void salaerLimitNotOverTenThousand()
	{
		Salaerkonto salaerkonto = new Salaerkonto("Thomas Seibert", 0, 10001);
		
		Assert.assertEquals(1000000, salaerkonto.getUeberzugslimite());
	}
	
	@Test
	public void canNotWithdrawMoreThanLimit()
	{
		Salaerkonto salaerkonto = new Salaerkonto("Thomas Seibert", 0, 500);
		salaerkonto.geldAbheben(2000);
		
		Assert.assertEquals(-50000,salaerkonto.getKontostand());
	}
	
	@Test
	public void nummerKontoNumberIncreasing()
	{
		Nummernkonto nk = new Nummernkonto(500);
		Nummernkonto nk1 = new Nummernkonto(500);
		
		Assert.assertEquals("Inhaber: 1001, Kontostand: 500.0", nk1.toString());
	}
}
