
public class Bankkonto 
{
	private final static int MAX_KONTOSTAND = 10000000;
	
	private String inhaber;
	private int kontostandInRappen;
	
	/**
	 * Erstellt ein Bankkonto mit inhaber und anfangs Kontostand
	 * @param inhaber
	 * @param kontostandInFranken
	 */
	public Bankkonto(String inhaber, double kontostandInFranken)
	{
		this.inhaber = inhaber;
		
		geldEinzahlen(kontostandInFranken);
	}
	
	/**
	 * Zahlt einen neuen Betrag ein. Der Maximalbetrag liegt bei 100'000 CHF
	 * @param betragInFranken
	 */
	public void geldEinzahlen(double betragInFranken)
	{
		int neuerBetrag = kontostandInRappen + convertToRappen(betragInFranken);
		
		if (neuerBetrag > MAX_KONTOSTAND) {
			kontostandInRappen = MAX_KONTOSTAND;
		} else {
			kontostandInRappen = neuerBetrag;
		}
	}
	
	/**
	 * Hebt einen Betrag ab. Kontostand darf nicht überzogen werden
	 * @param betragInFranken
	 */
	public void geldAbheben(double betragInFranken)
	{
		int neuerBetrag = kontostandInRappen - convertToRappen(betragInFranken);
		
		if (neuerBetrag < 0) {
			kontostandInRappen = 0;
		} else {
			kontostandInRappen = neuerBetrag;
		}
	}
	
	@Override
	public String toString()
	{
		return "Inhaber: " + inhaber + ", Kontostand: " + convertToFranken(kontostandInRappen);
	}
	
	protected int getKontostand()
	{
		return kontostandInRappen;
	}
	
	protected void setKontostand(int neuerKontostandInRappen)
	{
		kontostandInRappen = neuerKontostandInRappen;
	}
	
	protected double convertToFranken(int rappen)
	{
		return ((double)rappen / 100);
	}

	protected int convertToRappen(double franken)
	{
		return (int)(franken * 100);
	}
}
