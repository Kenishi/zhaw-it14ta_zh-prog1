
public class Bankverwaltung 
{
	public static void main(String[] argv)
	{
		// Erzeuge Bankkonten
		Bankkonto bk1 = new Bankkonto("Thomas Seibert", 0.5);
		Bankkonto bk2 = new Bankkonto("Peter Brandt", 102000);
		Bankkonto bk3 = new Bankkonto("Peter Steinhof", 99000);
		
		// Erzeuge Salärkonten
		Bankkonto bk4 = new Salaerkonto("Mark Sway", 0, 5000);
		Bankkonto bk5 = new Salaerkonto("Gary Bowen", 3000, 0);
		Bankkonto bk6 = new Salaerkonto("Ingar Mazu", 0, 10000);

		// Erzeuge Nummernkonto
		Bankkonto bk7 = new Nummernkonto(0);
		Bankkonto bk8 = new Nummernkonto(3000.55);
		Bankkonto bk9 = new Nummernkonto(2500.30);
		
		// Abheben + Einzahlen
		bk1.geldEinzahlen(105000.55);
		bk2.geldAbheben(102000);
		
		bk4.geldAbheben(2000);
		bk5.geldAbheben(3001);
		bk6.geldAbheben(9999);
		
		System.out.println("Bankkonten:");
		System.out.println(bk1);
		System.out.println(bk2);
		System.out.println(bk3);
		
		System.out.println("Salärkonten:");
		System.out.println(bk4);
		System.out.println(bk5);
		System.out.println(bk6);
		
		System.out.println("Nummernkonto:");
		System.out.println(bk7);
		System.out.println(bk8);
		System.out.println(bk9);
		
	}
}
