
public class Salaerkonto extends Bankkonto 
{
	private int ueberzugslimite;
	
	/**
	 * Erzeugt ein neues Konto
	 * @param inhaber	Inhaber Name
	 * @param kontostandInFranken anfangs Kontostand in Franken
	 * @param ueberzugslimite Überzugslimite muss zwischen 0 und 10000 CHF liegen
	 */
	public Salaerkonto(String inhaber, double kontostandInFranken, double ueberzugslimiteInFranken)
	{
		super(inhaber, kontostandInFranken);
		
		setUeberzugslimite(ueberzugslimiteInFranken);
	}
	
	/**
	 * Hebt einen Betrag ab. Kontostand darf bis zur Überzugslimite überzogen werden
	 * @param betragInFranken
	 */
	@Override
	public void geldAbheben(double betragInFranken)
	{
		int neuerBetrag = super.getKontostand() - super.convertToRappen(betragInFranken);
		
		if (neuerBetrag < (0 - ueberzugslimite)) {
			super.setKontostand((0 - ueberzugslimite));
		} else {
			super.setKontostand(neuerBetrag);
		}
	}
	
	@Override
	public String toString()
	{
		return super.toString() + ", Überzugslimite: " + super.convertToFranken(ueberzugslimite);
	}
	
	public int getUeberzugslimite()
	{
		return ueberzugslimite;
	}
	
	private void setUeberzugslimite(double ueberzugslimite)
	{
		if (ueberzugslimite < 0)
			this.ueberzugslimite = 0;
		else if(ueberzugslimite > 10000)
			this.ueberzugslimite = 1000000;
		else
			this.ueberzugslimite = super.convertToRappen(ueberzugslimite);
	}
}
