
public class Nummernkonto extends Bankkonto 
{
	private static int letzteKontonummer = 999;
	private int kontonummer;
	
	/**
	 * Erstellt ein Nummernkonto mit initialen Kontostand. Nummer wird fortlaufend nummeriert.
	 * @param kontostandInFranken
	 */
	public Nummernkonto(double kontostandInFranken)
	{
		super("", kontostandInFranken);
		kontonummer = ++letzteKontonummer;
	}
	
	@Override
	public String toString()
	{
		return "Inhaber: " + kontonummer + ", Kontostand: " + super.convertToFranken(super.getKontostand());
	}
}
