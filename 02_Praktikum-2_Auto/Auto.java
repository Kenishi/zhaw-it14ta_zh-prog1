
/**
 * Verwaltung des Lagerbestandes von Neufahrzeugen
 * 
 * @version (1.0)
 */
public class Auto
{
    // Statische Variabeln
    private static final int MIN_CHARS = 3;
    private static final int MAX_CHARS = 10;
    private static final double MIN_HUBRAUM = 0.4;
    private static final double MAX_HUBRAUM = 8;
    private static final int MAX_BESTAND = 10;
    
    private String marke;
    private String typ;
    private double hubraum;
    private boolean hatEinenTurbo;
    private int lagerbestand;
    
    /** Setzt die Automarke
     *  
     *  Die Marke muss min. 3 und maximal 
     */
    public void setMarke(String m) {
        if (pruefeStringLaenge(m,"Marke")) {
            marke = m;
        }
    }
    
    /** Setzt den Fahrzeugtyp
     * 
     */
    public void setTyp(String t) {
        if (pruefeStringLaenge(t,"Typ")) {
            typ = t;
        }
    }
    
    /** Setzt den Hubraum
     * 
     */
    public void setHubraum(double h)
    {
        if (pruefeHubraum(h)) {
            hubraum = h;
        }
    }
    
    /** Fuegt einen Turbo hinzu
     * 
     */
    public void fuegeTurboHinzu()
    {
        hatEinenTurbo = true;
    }
    
    /** Entfernt einen Turbo
     * 
     */
    public void entferneTurbo()
    {
        hatEinenTurbo = false;
    }
    
    /** Aendert den Bestand um max 10 nach oben oder nach unten
     * 
     */
    public void aendereBestand(int b)
    {
        if (b < -MAX_BESTAND || b > MAX_BESTAND) {
            System.out.println("Bestand darf maximal um " + MAX_BESTAND + " addiert oder subtrahiert werden");
        } else {
            
            System.out.println("Alter Bestand: " + lagerbestand);
            
            lagerbestand = lagerbestand + b;
            
            System.out.println("Neuer Bestand: " + lagerbestand);
        }
    }
    
    /** Zeige Auto Informationen
     * 
     */
    public void zeigeAuto() 
    {
        
        
        System.out.println("---------------------------------------");
        
        System.out.print(marke + " ");
        System.out.print(typ + ", ");
        System.out.println(hubraum + " Liter");
        
        System.out.println("Code: " + berechneCode());
        System.out.println("Lagerbestand: " + lagerbestand);
        System.out.println("---------------------------------------");
    }
    
    private String berechneCode() 
    {
        String turboCodeString = hatEinenTurbo ? "-t" : "";
        return marke.substring(0,3) + "-" + typ.substring(0,3) + turboCodeString;
    }
    
    /** Initialisiere ein Auto
     * 
     *  @param (String autoMarke min. 3 und max 10 Zeichen)
     *  @param (String autoTyp min. 3 und max 10 Zeichen)
     *  @param (double autoHubraum min 0.5 und max 8)
     *  @param (boolean mitTurbo)
     *  
     */
    public Auto(String autoMarke, String autoTyp, double autoHubraum, boolean mitTurbo) 
    {
        marke = pruefeStringLaenge(autoMarke, "Marke") ? autoMarke : "___";
        typ = pruefeStringLaenge(autoTyp, "Typ") ? autoTyp : "___";
        hubraum = pruefeHubraum(autoHubraum) ? autoHubraum : 0;
        hatEinenTurbo = mitTurbo;
        lagerbestand = 0;
    }
    
       
    /* Hubraum muss zwischen 0.5 und 8 sein */
    private boolean pruefeHubraum(double h) 
    {
        if (h >= MIN_HUBRAUM && h <= MAX_HUBRAUM) {
            return true;
        }
            
        printError("Hubraum muss min. " + MIN_HUBRAUM + " und max " + MAX_HUBRAUM + " sein");
        return false;
    }
    
    /* String muss min. 3 und höchstens 10 Zeichen lang sein */
    private boolean pruefeStringLaenge(String s, String feld) 
    {
        if (s.length() >= MIN_CHARS && s.length() <= MAX_CHARS) {
            return true;
        }
        
        printError(feld + " muss zwischen " + MIN_CHARS + " und " + MAX_CHARS + " Zeichen sein");
        return false;
    }
    
    private void printError(String error) 
    {
        System.out.println(error);    
    }
    
}
