
/**
 * Künstler
 * 
 */
public class Kuenstler
{
   private String name;
   private int gage;
   
   /**
    * Erstelle Künstler
    */
   public Kuenstler(String name, int gage)
   {
       this.name = name;
       this.gage = gage;
   }
   
   public String gebeName()
   {
       return name;
   }
   
   public int gebeGage()
   {
       return gage;
   }
}
