import java.util.ArrayList;
/**
 * Neue Events können erstellt werden
 * 
 */
public class Event
{
    private Kuenstler kuenstler;
    private ArrayList<TicketKategorie> ticketKategorien;
    
    /**
     * Macht nichts
     */
    public Event()
    {
        
    }
    
    /**
     * Erstellt einen Event
     * 
     * @param Künstler
     * @param Gage
     * @param VIP Ticket Preis
     * @param VIP Ticket Anzahl
     * @param Tribühnen Ticket Preis
     * @param Tribühnen Ticket Anzahl
     * @param Innenraum Ticket Preis
     * @param Innenraum Ticket Anzahl
     */
    public Event(String kuenstler, 
                int gage, 
                int vipTicketPreis, 
                int vipTicketAnzahl, 
                int tribuenenPreis,
                int tribuenenAnzahl,
                int innenraumPreis,
                int innenraumAnzahl)
    {
        setzeKuenstler(kuenstler, gage);
        ticketKategorien = new ArrayList<TicketKategorie>();
        
        setzeTicketKategorie("VIP-Tickets", vipTicketPreis, vipTicketAnzahl);
        setzeTicketKategorie("Tribünen-Tickets", tribuenenPreis, tribuenenAnzahl);
        setzeTicketKategorie("Innenraum-Tickets", innenraumPreis, innenraumAnzahl);
    }
    
    /**
     * Zeige Event Informationen
     */
    public void zeigeEventInformationen()
    {
        int gesamteinnahmen = 0;
        int gewinn = 0;
        
        System.out.println("Kuenstler: " + kuenstler.gebeName() + ", Gage: CHF " + kuenstler.gebeGage());
        
        for(TicketKategorie kategorie : ticketKategorien) {
            kategorie.zeigeTicketInformationen();
            gesamteinnahmen += kategorie.gebeEinnahmen();
        }
        
        System.out.println("Gesamteinnahmen: CHF " + gesamteinnahmen);
        
        gewinn = gesamteinnahmen - kuenstler.gebeGage();
        System.out.println("Gewinn: CHF " + gewinn);
    }
    
    /**
     * Setzt einen neuen Künstler mit Bezeichnung und Gage
     */
    public void setzeKuenstler(String kuenstlerName, int gage)
    {
        this.kuenstler = new Kuenstler(kuenstlerName, gage);
    }
    
    /**
     * Kaufe Ticket nach Kategorie
     * 1 = VIP Ticket
     * 2 = Tribünen Ticket
     * 3 = Innenraum Ticket
     */
    public void kaufeTicket(int kategorie)
    {
        TicketKategorie kat = ticketKategorien.get(kategorie-1);
        kat.verkaufeTicket();
    }
    
    private void setzeTicketKategorie(String kategorieName, int ticketPreis, int anzahlTickets)
    {
        ticketKategorien.add(new TicketKategorie(kategorieName, ticketPreis, anzahlTickets));
    }
}
