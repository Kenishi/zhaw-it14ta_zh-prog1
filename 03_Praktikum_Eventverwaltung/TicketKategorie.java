
/**
 * Dient zur Verwaltung von Tickets verkauf
 * 
 */
public class TicketKategorie
{
    private String kategorieBezeichnung;
    private int preisProTicket;
    private int anzahlDerTickets;
    private int verkaufteTickets;
    
    /**
     * Zeigt TicketKategorie Informationen
     */
    public void zeigeTicketInformationen()
    {
        System.out.println(kategorieBezeichnung + ": " 
        + verkaufteTickets + " von "
        + anzahlDerTickets + " verkauft, "
        + "Einnahmen: CHF " + verkaufteTickets *  preisProTicket);
    }
    
    /**
     * Verkaufe Ticket
     */
    public void verkaufeTicket()
    {
        if (!hatEsNochTickets()) {
            System.out.println("Alle " + kategorieBezeichnung + " verkauft");
        } else {
            verkaufteTickets += 1;
        }
    }
    
    /** 
     * Erstellt neue Ticketkategorie
     */
    public TicketKategorie(String kategorieBezeichnung, int preisProTicket, int anzahlDerTickets)
    {
        setzeBezeichnung(kategorieBezeichnung);
        setzePreisProTicket(preisProTicket);
        setzeAnzahlDerTickets(anzahlDerTickets);
        this.verkaufteTickets = 0;
    }
    
    /**
     * Sind noch tickets verfügbar
     */
    public boolean hatEsNochTickets()
    {
        if (verkaufteTickets == anzahlDerTickets)
        {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Gebe Einnahmen
     */
    public int gebeEinnahmen()
    {
        return preisProTicket * verkaufteTickets;
    }
    
    private void setzeBezeichnung(String bezeichnung)
    {
        if (bezeichnung.length() > 0) {
            this.kategorieBezeichnung = bezeichnung;
        } else {
            System.out.println("Bezeichnung darf nicht leer sein");
        }
    }
    
    private void setzePreisProTicket(int preis) 
    {
        if (preis > 0) {
            preisProTicket = preis;
        } else {
            System.out.println("Preis muss höher sein als 0");
        } 
    }
    
    private void setzeAnzahlDerTickets(int anzahl)
    {
        if (anzahl > 0) {
            anzahlDerTickets = anzahl;
        } else {
            System.out.println("Anzahl der Tickets muss höher sein als 0");
        }
    }
}
