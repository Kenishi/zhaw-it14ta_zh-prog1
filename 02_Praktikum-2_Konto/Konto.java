/**
 * Konto klasse
 * 
 * @version (1.0)
 */
public class Konto
{
   private static final int MIN_ZEICHEN = 3;
   private static final int MAX_ZEICHEN = 20;
   private static final int MIN_ZINS = 0;
   private static final int MAX_ZINS = 10;
   private static final int MAX_BETRAG = 10000;
   
   private String kontoinhaber;
   private int kontostand;
   private int zinssatz;
 
   /** Initialisiert ein neues Konto mit einem Kontoinhaber
    * 
    *  @param (String kontoInhaber)
    */
   public Konto(String kontoInhaber) 
   {
       setzeKontoInhaber(kontoInhaber);
       zinssatz = 2;
       kontostand = 0;
   }
    
   /** Initialisiert ein neues Konto mit Kontoinhaber und Zinssatz
    * 
    *  @param (String kontoInhaber)
    *  @parmm (int zins)
    */
   public Konto(String kontoInhaber, int zins) 
   {
       setzeKontoInhaber(kontoInhaber);
       setzeZins(zins);
       kontostand = 0;
   }
   
   private void setzeKontoInhaber(String kontoInhaber) 
   {
       kontoinhaber = pruefeLaengeVonKontoinhaber(kontoInhaber) ? kontoInhaber : "no name";
   }
   
   private boolean pruefeLaengeVonKontoinhaber(String text)
   {
       if (text.length() < MIN_ZEICHEN || text.length() > MAX_ZEICHEN)
       {
           System.out.println("Kontoinhaber muss min. " + MIN_ZEICHEN + " und Maximum " + MAX_ZEICHEN + " haben");
           return false;
       } 
       
       return true;
   }
   
   private void setzeZins(int zins)
   {
       zinssatz = pruefeZins(zins) ? zins : 0;
   }
   
   private boolean pruefeZins(int zins)
   {
       if (zins < MIN_ZINS || zins > MAX_ZINS)
       {
           System.out.println("Zinssatz muss min. " + MIN_ZINS + " und Maximum " + MAX_ZINS + " haben");
           return false;
       } 
       
       return true;
   }
   
   /** Gibt den aktuellen Kontostand zurück
    * 
    */
   public int getKontostand() 
   {
       return kontostand;
   }
   
   /** Setzt einen neuen Kontostand
    *  
    *  @param (int neuerKontostand)
    */
   public void setKontostand(int neuerKontostand) 
   {
       kontostand = neuerKontostand;
   }
   
   /** Fuegt den Betrag zum aktuellen Kontostand hinzu
    * 
    *  @param (int betrag)
    */
   public void geldEinzahlen(int betrag) 
   {
       if (istBetragPositivUndNichtMax(betrag)) {
           kontostand = kontostand + betrag;
       }
   }
   
   /** Hebt einen Betrag vom Kontostand ab
    * 
    *  @param (int betrag)
    */
   public void geldAbheben(int betrag) 
   {
       if (istBetragPositivUndNichtMax(betrag) && hatGenugGeld(betrag)) {
           kontostand = kontostand - betrag;
       }
   }
   
   private boolean hatGenugGeld(int betrag) 
   {
       if (betrag > kontostand) {
           System.out.println("Nicht genügend Geld auf dem Konto. Aktueller Kontostand: " + kontostand);
           return false;
       } else {
           return true;
       }
   }
       
   
   private boolean istBetragPositivUndNichtMax(int betrag) 
   {
       if (betrag <= 0) {
           System.out.println("Der Betrag muss grösser als 0 sein!");
       } else if (betrag > MAX_BETRAG) {
           System.out.println("Der Maximale Betrag darf höchstens 10'000 sein");
       } else {
           return true;
       }
        
       return false;
   }
   
   /** Berechnet den Zins des Kontostandes in Abhängigkeit des Zinssatzes
    * 
    */
   public int gibtZinsVonKontostand() 
   {
       return kontostand / 100 * zinssatz;
   }
   
   /** Zeigt diverse Kontoinformationen in der Konsole an
    * 
    */
   public void zeigeKontoInformationen() 
   {
       System.out.println();
       System.out.println("-------------------------------------------");
       System.out.println("Informationen zum Konto");
       System.out.println("Kontoinhaber: " + kontoinhaber);
       System.out.println("Kontostand: " + kontostand);
       System.out.println("Zinssatz: " + zinssatz + "%");   
   } 
}
