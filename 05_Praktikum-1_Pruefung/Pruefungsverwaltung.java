import java.util.HashMap;
import java.util.ArrayList;

/**
 * Bietet Funktionalitaeten zum Speichern von Pruefungsergebnissen von einer
 * Vielzahl von Studenten. Aus den gespeicherten Ergebnissen lassen sich
 * personalisierte Antworttext generieren.
 */
public class Pruefungsverwaltung {
    ArrayList<Pruefungsergebnis> pruefungsErgebnisse;
    HashMap<String, String> zahlInWorte;

    public Pruefungsverwaltung()
    {
        pruefungsErgebnisse = new ArrayList<>();
        zahlInWorte = new HashMap<>();

        zahlInWorte.put("1","eins");
        zahlInWorte.put("2","zwei");
        zahlInWorte.put("3","drei");
        zahlInWorte.put("4","vier");
        zahlInWorte.put("5","fuenf");
    }

    /**
     * Speichert ein Pruefungsergebnis.
     * 
     * @param ergebnis Das Pruefungsergebnis.
     */
    public void speichereErgebnis(Pruefungsergebnis ergebnis) {

        pruefungsErgebnisse.add(ergebnis);
    }

    /**
     * Gibt pro gespeichert Ergebnis einen Text auf die Konsole aus.
     * Je nachdem ob der Kandidate die Pruefung bestanden (>= 4.0) oder nicht
     * bestanden (< 4.0) hat, wird ein Text in folgendem Format ausgegeben:
     * 
     * Sie haben an der Pruefung eine 3.0 (drei punkt null) erzielt und 
     * sind somit durchgefallen!
     * 
     * Herzliche Gratulation Max Muster! Sie haben an der Pruefung eine 4.5
     * (vier pounkt fuenf) erzielt und somit bestanden!
     */
    public void druckeAntworttexte() {

        System.out.println();
        for(Pruefungsergebnis ergebnis : pruefungsErgebnisse)
        {
            double note = rundeAufHalbeNote(ergebnis.getNote());

            if (hatBestanden(note)) {
                System.out.print("Herzliche Gratulation " + ergebnis.getStudent() + "! ");
            } else {
                System.out.print(ergebnis.getStudent() + ". ");
            }

            System.out.print("Sie haben an der Pruefung eine " + note);
            System.out.print(convertNumberInWords(note));
            System.out.print(" erzielt und ");

            if (note >= 4) {
                System.out.print("somit  bestanden!");
            } else {
                System.out.print("sind somit durchgefallen!");
            }
            System.out.println();
        }
    }

    private boolean hatBestanden(double note) {
        if (note >= 4) {
            return true;
        }

        return false;
    }

    private String convertNumberInWords(double note) {
        String[] notenString = Double.toString(Math.abs(note)).split("\\.");

        return " (" + zahlInWorte.get(notenString[0]) + " punkt " + zahlInWorte.get(notenString[1]) + ")";
    }

    private double rundeAufHalbeNote(double note) {
        return Math.round(note * 2) / 2.0;
    }
}
