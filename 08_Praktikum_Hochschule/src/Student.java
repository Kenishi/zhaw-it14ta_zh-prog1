/**
 * Der Student besitzt creditspunkte welche erhöht werden können
 * @author chris
 *
 */
public class Student extends Person
{
	private int credits;
	
	/**
	 * Erstellt einen Studenten mit name und id
	 * @param name Name des Studenten
	 * @param id ID Nummer des Studenten
	 */
	public Student(String name, String id)
	{
		super(name, id);
	}
	
	/**
	 * Gibt aktueller Creditstand zurück
	 * @return creditstand
	 */
	public int gibCredits()
	{
		return credits;
	}
	
	/**
	 * Erhöht den credit um die angegebene ganze Zahl
	 * @param creditZuErhoehen
	 */
	public void erhoeheCredits(int creditZuErhoehen)
	{
		credits += creditZuErhoehen;
	}
}
