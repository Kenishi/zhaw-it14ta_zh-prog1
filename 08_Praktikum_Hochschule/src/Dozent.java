/**
 * Dozent mit name, vorname, buero und telefonnummer
 * @author chris
 *
 */
public class Dozent extends Person {
	private String bueronummer;
	private String telefonnummer;
	
	/**
	 * Erstellt einen Dozenten
	 * @param name	Name des Dozenten
	 * @param id	ID des Dozenten
	 * @param bueronummer	Büronummer des Dozenten 
	 * @param telefonnummer Telefonnummer des Dozenten
	 */
	public Dozent(String name, String id, String bueronummer, String telefonnummer) {
		super(name, id);
		this.bueronummer = bueronummer;
		this.telefonnummer = telefonnummer;
	}
	
	/**
	 * Gibt das aktuelle Büro des Dozenten zurück
	 * @return Büronummer
	 */
	public String gibBuero() {
		return bueronummer;
	}

	/**
	 * Gibt die Telefonnummer zurück
	 * @return Telefonnummer
	 */
	public String gibTelefonnummer() {
		return telefonnummer;
	}
}
