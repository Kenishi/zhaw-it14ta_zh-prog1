/**
 * Die Person enthält globale felder wie name und id sowie eine funktion um die Informationen auszugeben
 * @author chris
 *
 */
public class Person {
	private String name;
	private String id;
	
	/**
	 * Erstellt eine Person mit name und id
	 * @param name	Name der Person
	 * @param id 	Id der Person
	 */
	public Person(String name, String id)
	{
		this.name = name;
		this.id = id;
	}
	
	/**
	 * Gibt Name und ID als String zurück
	 * @return Name und ID als String
	 */
	public String gibInfo()
	{
		return name + ", ID " + id;
	}
}
