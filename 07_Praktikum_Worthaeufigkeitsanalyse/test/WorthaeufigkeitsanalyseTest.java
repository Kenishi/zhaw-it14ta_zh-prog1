import org.junit.*;
import java.util.HashSet;
import java.util.Iterator;

public class WorthaeufigkeitsanalyseTest {
	Worthaeufigkeitsanalyse wortAnalyse;
	
	@Before
	public void setup()
	{
		wortAnalyse = new Worthaeufigkeitsanalyse();
	}
	
	@Test
	public void checkIfAllPunctuationGetsReplaced()
	{
		HashSet<String> punctuations = wortAnalyse.gibSatzzeichen();
		
		Iterator<String> it = punctuations.iterator();
		
		boolean hasBeenReplaced = true;
		
		while(it.hasNext() && hasBeenReplaced)
		{
			String punctuation = it.next();
			hasBeenReplaced = isReplaced(punctuation); 
		}
		
		Assert.assertTrue(hasBeenReplaced);
	}
	
	@Test
	public void checkIfTwoWordsAreSaved()
	{
		Assert.assertEquals(wortAnalyse.gibWoerterHaeufigkeit("hallo velo").size(),2);
	}
	
	@Test
	public void checkIfTwoSameWordsAreCounted()
	{
		Assert.assertEquals(wortAnalyse.gibWoerterHaeufigkeit("hallo hallo").get("hallo").intValue(), 2);
	}
	
	@Test
	public void checkIfOneWordIsSaved()
	{
		Assert.assertEquals(wortAnalyse.gibWoerterHaeufigkeit("test ").size(), 1);
	}
	
	@Test
	public void checkIfEmptyWordIsNotSaved()
	{
		Assert.assertEquals(wortAnalyse.gibWoerterHaeufigkeit(" ").size(), 0);
	}
	
	@Test(expected = NullPointerException.class)
	public void checkIfNullThrowsException()
	{
		Assert.assertEquals(wortAnalyse.gibWoerterHaeufigkeit(null).size(), 0);
	}
	
	
	private boolean isReplaced(String input)
	{
		if (wortAnalyse.entferneSatzzeichen(input).equals("")) {
			return true;
		} else {
			return false;
		}
	}
}
