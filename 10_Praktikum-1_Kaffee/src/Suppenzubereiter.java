/**
 * Diese Klasse bereitet eine Suppe zu.
 *
 * @author Kijudai, Timothy, Angelica
 */

import java.util.ArrayList;

public class Suppenzubereiter {
    public static void main(String[] argv) {
        ArrayList<Suppe> suppen = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            suppen.add(new Suppe());
            for (Suppe suppe: suppen) {
                suppe.bereiteZu();
                suppe.trinken();
            }
        }
    }

}
