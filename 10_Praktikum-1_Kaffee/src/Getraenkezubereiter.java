/**
 * Diese Klasse bereitet ein Getraenk zu.
 *
 * @author Kijudai, Timothy, Angelica
 */

import java.util.ArrayList;

public class Getraenkezubereiter {
	public static void main(String[] argv) {
		ArrayList<KoffeinGetraenk> getraenke = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			getraenke.add(new Tee());
			getraenke.add(new Kaffee());
			for (KoffeinGetraenk getraenk : getraenke) {
				getraenk.bereiteZu();
				getraenk.trinken();
			}
		}
	}

}
