/**
 * Trinkbares wird serviert zum Trinken.
 *
 * @author Kijudai, Timothy, Angelica
 */
public interface Trinkbar
{
	/**
	 * Ein Getränk zum Trinken servieren.
	 */
	public void trinken();
}
