/**
 * Diese Klasse bietet die Funktionalitaet, um einen Kaffee zu kochen.
 * 
 * @author tebe
 */
public class Kaffee extends KoffeinGetraenk
{

	@Override
	public void braue()
	{
		System.out.println("Kaffee wird gebraut...");
	}

	@Override
	public void fuegeZutatenHinzu() {
		System.out.println("Fuege Zucker und Milch hinzu...");
	}
}
