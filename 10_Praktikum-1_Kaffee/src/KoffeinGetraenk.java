/**
 * Diese Klasse bereitet ein Koffeingetraenk zu
 *
 * @author Kijudai, Timothy, Angelica
 */
public abstract class KoffeinGetraenk implements Trinkbar
{
	/**
	 * Bereitet ein Getränk zu
	 */
	public final void bereiteZu()
	{
		System.out.println("Bereite einen " + this.getClass().getSimpleName() + " zu:");
		kocheWasser();
		braue();
		giesseInTasse();
		fuegeZutatenHinzu();
	}

	/**
	 * Wasser kochen.
	 */
	private void kocheWasser() {
		System.out.println("Wasser kocht...");
	}

	/**
	 * Flüssigkeit in Tasse giessen.
	 */
	private void giesseInTasse()
	{
		System.out.println("Getränk wird in Tasse gegossen...");
	}

	/**
	 * Getränk kann getrunken werden.
	 */
	@Override
	public void trinken()
	{
		System.out.println("=> Das Getränk " + this.getClass().getSimpleName() + " wird getrunken!");
		System.out.println();
	}

	abstract public void braue();
	abstract public void fuegeZutatenHinzu();
}
