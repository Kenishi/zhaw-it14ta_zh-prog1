/**
 * Diese Klasse bereitet eine Suppe zu
 *
 * @author Kijudai, Timothy, Angelica
 */
public class Suppe implements Trinkbar
{
    /**
     * Bereitet eine Suppe zu
     */
    public final void bereiteZu()
    {
        System.out.println("Bereite eine " + this.getClass().getSimpleName() + " zu:");
        kocheWasser();
        fuegeZutatenHinzu();
        giesseInTeller();

    }

    /**
     * Wasser kochen.
     */
    private void kocheWasser() {
        System.out.println("Wasser kocht...");
    }

    /**
     * Flüssigkeit in Tasse giessen.
     */
    private void giesseInTeller()
    {
        System.out.println("Suppe wird in den Teller gegossen...");
    }

    /**
     * Suppe kann getrunken werden.
     */
    @Override
    public void trinken()
    {
        System.out.println("=> " + this.getClass().getSimpleName() + " wird getrunken!");
        System.out.println();
    }

    public void fuegeZutatenHinzu()
    {
        System.out.println("Öffne Suppenbeutel...");
        System.out.println("Leere Suppenbeutel in das Wasser...");
    }
}

