/**
 * Diese Klasse bietet die Funktionalitaet, um einen Tee zu kochen.
 * 
 * @author tebe
 */
public class Tee extends KoffeinGetraenk
{
	/**
	 * Brauen = Teebeutel in Tasse legen.
	 */
	@Override
	public void braue()
	{
		System.out.println("Teebeutel wird in Tasse gelegt...");
	}

	/**
	 * Zutat Zitrone beilegen.
	 */
	@Override
	public void fuegeZutatenHinzu()
	{
		System.out.println("Zitrone wird beigefügt...");
	}
}
