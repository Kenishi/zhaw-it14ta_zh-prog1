import java.awt.Point;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * Spielklasse des Spiels Snake.
 *
 * Ziel dieses Spiels ist es alle Goldstuecke einzusammeln und 
 * die Tuer zu erreichen, ohne sich selber zu beissen oder in 
 * die Spielfeldbegrenzung reinzukriechen. 
 */
public class SnakeSpiel {
  private Schlange schlange;
  private Tuer tuer;
  private Spielfeld spielfeld;
  private ArrayList<Point> goldStuecke;
  private boolean spielLaeuft = true;

  /**
   * Startet das Spiel.
   */
  public void spielen(int anzahlGoldstuecke) {
    spielInitialisieren(anzahlGoldstuecke);
    while (spielLaeuft) {
      zeichneSpielfeld();
      ueberpruefeSpielstatus();
      fuehreSpielzugAus();
    }   
  }
  
  public static void main(String[] args, int anzahlGoldStuecke) {
    new SnakeSpiel().spielen(anzahlGoldStuecke);
  }

  private void spielInitialisieren(int anzahlGoldstuecke) {
    tuer = new Tuer(0, 5);
    spielfeld = new Spielfeld(40, 10);
    setAnzahlGoldstuecke(anzahlGoldstuecke);
    schlange = new Schlange(30, 2);
  }
  
  private void setAnzahlGoldstuecke(int anzahl) {
      if(anzahl == 0) {
          anzahl = 10;
      }
      goldStuecke = new ArrayList<Point>();
      for(int i = 0; i < anzahl;i++) {
          goldStuecke.add(new Point (spielfeld.erzeugeZufallspunktInnerhalb()));
      }
  }
  
  private void zeichneSpielfeld() {
    char ausgabeZeichen;
    for (int y = 0; y < spielfeld.gibHoehe(); y++) {
      for (int x = 0; x < spielfeld.gibBreite(); x++) {
        Point punkt = new Point(x, y);
        ausgabeZeichen = '.';
        if (schlange.istAufPunkt(punkt)) {
          ausgabeZeichen = '@';
        } else if (istEinGoldstueckAufPunkt(punkt, false)) {
          ausgabeZeichen = '$';
        } else if (tuer.istAufPunkt(punkt)) {
          ausgabeZeichen = '#';
        } 
        if(schlange.istKopfAufPunkt(punkt)) {
          ausgabeZeichen = 'S';         
        }
        System.out.print(ausgabeZeichen);
      }
      System.out.println();
    }
  }
  
  private boolean istEinGoldstueckAufPunkt(Point punkt, boolean loeschen) {
    int index = 0;
    boolean amSuchen = true;
    while(amSuchen && index < goldStuecke.size()) {
      Point indexPunkt = goldStuecke.get(index);
      if(indexPunkt.equals(punkt)) {
        amSuchen = false;
        if(loeschen) {
          goldStuecke.remove(indexPunkt);
        }
        return true;
      }
      else {
        index++;
      }
    }
    return false;
  }
  
  private void ueberpruefeSpielstatus() {
    if (istEinGoldstueckAufPunkt(schlange.gibPosition(), true)) {
      int wertGoldStueck = (int)(1 + Math.random() * 5);
      schlange.wachsen(wertGoldStueck);
      System.out.println("Goldstueck im Wert von " + wertGoldStueck + " eingesammelt.");
    }
    if (istVerloren()){
      System.out.println("Verloren!");
      spielLaeuft = false;
    }
    if (istGewonnen()){
      System.out.println("Gewonnen!");
      spielLaeuft = false;
    }
  }
  
  private boolean istGewonnen() {
    return goldStuecke.size() == 0 && 
      tuer.istAufPunkt(schlange.gibPosition());
  }

  private boolean istVerloren() {
    return schlange.istKopfAufKoerper() || 
         !spielfeld.istPunktInSpielfeld(schlange.gibPosition());
  }
  
  private void fuehreSpielzugAus() {
    char eingabe = liesZeichenVonTastatur();
    schlange.bewege(eingabe);
  }

  private char liesZeichenVonTastatur() {
    Scanner scanner = new Scanner(System.in);
    char konsolenEingabe = scanner.next().charAt(0);
    return konsolenEingabe;
  }

}