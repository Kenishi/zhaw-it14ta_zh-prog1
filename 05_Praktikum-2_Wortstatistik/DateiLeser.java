import java.io.*;
import java.util.ArrayList;
/**
 * Write a description of class DateiLeser here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DateiLeser
{
    ArrayList<String> dateiZeilen;
    String datei;
    
    /**
     * Liest eine Datei ein und speichert jede Zeile in ein ArrayList item
     * 
     * @param datei - Erwartet vollen pfad zu einer Datei
     */
    public DateiLeser(String datei)
    {
        dateiZeilen = new ArrayList<>();
        this.datei = datei;
        
        convertFileToArray();
    }
    
    private void convertFileToArray()
    {
        try {
            Reader reader = new InputStreamReader(new FileInputStream(datei), "UTF-8");
            BufferedReader buffer = new BufferedReader(reader);
            String zeile = buffer.readLine();
            
            while (zeile != null)
            {
                dateiZeilen.add(zeile);
                zeile = buffer.readLine();
            }

            buffer.close();
        } catch(IOException e) {
            System.out.println("IO Exception " + e.getMessage());
        }
    }
    
    public ArrayList<String> gibDateiZeilen()
    {
        return dateiZeilen;
    }
}
