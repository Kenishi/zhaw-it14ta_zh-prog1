import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.ArrayList;

/**
 * Zaehlt die Anzahl Vorkommnisse von Woertern ueber mehrere Zeichenketten.
 * Es lassen sich eine beliebige Anzahl an Zeichenketten uebergeben. Die
 * Statistik wird fortlaufend gefuehrt. Die Wortzaehlung laesst sich jederzeit
 * ausgeben. Die Satzeichen . , ? ! " : ; werden entfernt. Alle Buchstaben
 * werden in Kleinbuchstaben umgewandelt um beispielsweise das Wort 'die'
 * inmitten eines Satzes und das Wort 'Die' am Anfang eines Satzes als gleiches
 * Wort werten zu koennen.
 * 
 * @version 1.0
 */
public class Worthaeufigkeitsanalyse {
    WortKontainer container;
    

    public Worthaeufigkeitsanalyse()
    {
        container = new WortKontainer();
    }

    /**
     * Method verarbeiteText wird die Zeichenkette in einzelne Wörter und Buchstaben unterteilen
     *
     * @param zeichenkette Eine Zeichenkette als string
     */
    public void verarbeiteText(String zeichenkette)
    {
        container.verarbeiteText(zeichenkette);
    }

    /**
     * Method druckeStatistik zeit die Statistik der Wörter und Buchstaben an
     *
     */
    public void druckeStatistik()
    {
        druckeWoerter();
        druckeZeichen();
    }

    private void druckeWoerter()
    {
        System.out.format("%20s %3s\n", "Wort","Anz");
        System.out.println("---------------------------");
        for (Map.Entry<String, Integer> entry : container.getWorte().entrySet()) {
            System.out.format("%20s %3d\n", entry.getKey(), entry.getValue().intValue());
        }
    }

    private void druckeZeichen()
    {
        System.out.format("%20s %3s %3s\n", "Zeichen","Anz","%");
        System.out.println("---------------------------");

        SortedSet<String> keys = new TreeSet<String>(container.getBuchstaben().keySet());

        for (String key : keys)
        {
            double prozent = (100d / container.getMaxmimumCharAnzahl()) * container.getBuchstaben().get(key).doubleValue();
            System.out.format("%20s %3d %3.1f %s\n", key.toUpperCase(), container.getBuchstaben().get(key).intValue(), prozent, "%");
        }
    }

    /**
     * Method vonDateiEinlesen
     *
     * @param dateiPfad Erwartet ein voller Dateipfad als String
     */
    public void vonDateiEinlesen(String dateiPfad)
    {
        DateiLeser dateiLeser = new DateiLeser(dateiPfad);
        ArrayList<String> zeilen = dateiLeser.gibDateiZeilen();

        for(String zeile : zeilen) {
            verarbeiteText(zeile);
        }

    }
}
