import java.util.HashMap;
/**
 * Speichert Worte und Buchstaben
 * 
 * @version 1.0
 */
public class WortKontainer
{
    HashMap<String, Integer> worte;
    HashMap<String, Integer> buchstaben;
    int maximumWortAnzahl;
    int maximumCharAnzahl;
    static char[] toIgnore;

    static {
        toIgnore = ".,?!\":;-()/".toCharArray();
    }
    
    public WortKontainer()
    {
        worte = new HashMap<>();
        buchstaben = new HashMap<>();
        int maximumWortAnzahl = 0;
        int maximumCharAnzahl = 0;
    }
    
    public HashMap<String, Integer> getWorte()
    {
        return worte;
    }
    
    public HashMap<String, Integer> getBuchstaben()
    {
        return buchstaben;
    }
    
    public int getMaxmimumWortAnzahl()
    {
        return maximumWortAnzahl;
    }
    
    public int getMaxmimumCharAnzahl()
    {
        return maximumCharAnzahl;
    }
    
    /**
     * Verarbeitet einen Textstring in einzelne Worte und Buchstaben
     * Es eliminiert gewisse Sonderzeichen
     * 
     * @param zeichenkette - Einen beliebig langen String
     */
    public void verarbeiteText(String zeichenkette)
    {
        String[] words = entferneIgnorierteCharakter(zeichenkette).split("\\s+");
        for(String word : words) {
            verwalteHashMap(worte, word);
            maximumWortAnzahl++;
            
            char[] chars = word.toCharArray();
            for(char buchstabe : chars) {
                verwalteHashMap(buchstaben, "" + buchstabe);
                maximumCharAnzahl++;
            }
        }
    }

    private void verwalteHashMap(HashMap<String, Integer> hash, String key)
    {
        key = key.toLowerCase();
        Integer count;
        if(hash.containsKey(key)) {
            count = new Integer(hash.get(key).intValue() + 1);
        } else {
            count = new Integer(1);
        }

        hash.put(key,count);
    }

    private String entferneIgnorierteCharakter(String zeichenkette)
    {
        String retval = zeichenkette;

        for(char ignore : toIgnore) {
            retval = retval.replace(ignore,' ');
        }

        return retval;
    }
}
