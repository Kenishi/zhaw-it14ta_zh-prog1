import java.util.Arrays;

/**
 * Die Klasse MessApp steuert einen Messablauf, um die Performance des
 * Zufallszahlengenerators zu messen. 
 */
public class MessApp {
  private final static int MESSREIHEN = 10;
  private final static int MESSUNGEN = 20;
  private final static int ZUFALLSZAHLEN = 400000;
  
  private Messkonduktor messkonduktor;
  private int[][] messreihen;

  /**
   * Fuehrt eine Messung durch.
   */
  public void messen() {
    initialisieren();
    analyseDurchfuehren();
    berechneUndDruckeMittelwerteMessreihe();
    berechneUndDruckeMittelwerteMessung();
  }

  private void initialisieren() {
    messkonduktor = new Messkonduktor(ZUFALLSZAHLEN);
  }

  private void analyseDurchfuehren() {
      messreihen = new int[MESSREIHEN][MESSUNGEN];
      
      for(int reihe=0; reihe < messreihen.length; reihe++)
      {
          messreihen[reihe] = messkonduktor.messungenDurchfuehren(messreihen[reihe]);
      }
  }

  private void berechneUndDruckeMittelwerteMessreihe() 
  {
      for(int reihe=0; reihe<messreihen.length; reihe++) {
          double summeDerMessungen = 0;
          
          for(int messung=0; messung<messreihen[reihe].length; messung++) {
              summeDerMessungen += messreihen[reihe][messung];
          }
          
          System.out.println("Messreihe " + reihe + " Durchschnitt: " + summeDerMessungen/messreihen[reihe].length + " ms");
      }
  }

  private void berechneUndDruckeMittelwerteMessung() {
      double[] summeProMessung = new double[MESSUNGEN];
      
      for(int reihe=0; reihe<messreihen.length; reihe++) {
          for(int messung=0; messung<messreihen[reihe].length; messung++) {
              summeProMessung[messung] += messreihen[reihe][messung];
          }
     
      }
      
      for(int mittel=0; mittel<summeProMessung.length; mittel++) {
          System.out.println("Messung " + mittel + " Durchschnitt: " + summeProMessung[mittel] / MESSREIHEN + " ms");
      }
  }
}
