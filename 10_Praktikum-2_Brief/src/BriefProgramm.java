import java.util.ArrayList;
import java.util.Calendar;

/**
 * Diese Klasse implementiert ein Briefprogramm.
 * 
 * @author tebe
 */
public class BriefProgramm {

  /**
   * main Methode.
   * @param args Es werden keine Kommandozeilenparameter verwendet
   */
	public static void main(String[] args) {

		Inhalt inhalt = new Inhalt(Calendar.getInstance(),"Ein Titel","Eine Anrede","In super text");
		Adresse sender = new Adresse("Thomas", "Seibert", "J.-C.Heerstr", 25, 8635, "Dürnten");
		
		Briefdrucker briefdrucker = new Briefdrucker(new FensterBriefdruckStrategie(), new KonsoleDruckStrategie(),inhalt, sender);
		briefdrucker.druckeSerienbrief(generiereAdressen(20));
	}
	
	private static ArrayList<Adresse> generiereAdressen(int anzahl)
	{
		ArrayList<Adresse> retval = new ArrayList<>();
		
		for(int i=0; i<anzahl; i++)
		{
			retval.add(new Adresse("Hans " + i, "Muster", "Seestr.", i, 8635 + i, "Dürnten"));
		}
		
		return retval;
	}
}