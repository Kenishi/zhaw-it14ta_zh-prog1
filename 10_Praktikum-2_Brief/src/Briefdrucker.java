import java.util.ArrayList;

/**
 * Diese Klasse implementiert einen Briefdrucker
 * 
 * @author tebe
 */
public class Briefdrucker 
{
	private DruckStrategie druckStrategie;
	private FormatStrategie formatStrategie;
	private Inhalt inhalt;
	private Adresse sender;
	
	public Briefdrucker(FormatStrategie formatStrategie, DruckStrategie druckStrategie, Inhalt inhalt, Adresse sender)
	{
		this.druckStrategie = druckStrategie;
		this.formatStrategie = formatStrategie;
		this.inhalt = inhalt;
		this.sender = sender;
	}
	
	public void druckeSerienbrief(ArrayList<Adresse> adressenEmpfaenger)
	{
		for(Adresse adresse : adressenEmpfaenger)
		{
			druckeBrief(adresse);
		}
	}
	
	public void druckeBrief(Adresse adresseEmpfaenger)
	{
		Brief brief = erstelleBriefMitEmpfaenger(adresseEmpfaenger);
		druckeBriefMitStrategie(brief);
	}
	
	private Brief erstelleBriefMitEmpfaenger(Adresse adresseEmpfaenger)
	{
		return new Brief(sender,adresseEmpfaenger,inhalt);
	}
	
	private void druckeBriefMitStrategie(Brief brief)
	{
		druckStrategie.drucken(formatStrategie.formatiereBrief(brief));
	}	
}
