import java.text.DateFormat;
import java.text.SimpleDateFormat;


public abstract class BriefFormatterFactory implements FormatStrategie
{	
	public String formatiereBrief(Brief brief)
	{
		StringBuilder retval = new StringBuilder();
		
		retval.append("*****************************************************\n");
		retval.append(erstelleDatum(brief));
		retval.append("--------------------------------\n");
		retval.append(formatiereEmpfaenger(brief));
		retval.append("--------------------------------\n");
		retval.append(formatiereSender(brief));
		retval.append("--------------------------------\n");
		retval.append(erstelleBriefinhalt(brief.getInhalt()));
		retval.append("*****************************************************\n");
		
		return retval.toString();
	}
	
	protected String erstelleDatum(Brief brief)
	{
		DateFormat dateFormater = new SimpleDateFormat("dd.MM.yyyy");
		String date = dateFormater.format(brief.getInhalt().getDatum().getTime());
		
		return "Datum: " + date + "\n";
	}
	
	protected String formatiereEmpfaenger(Brief brief)
	{
		return "Empfänger:\n" + erstelleAdresskopf(brief.getEmpfaenger());
	}
	
	protected String formatiereSender(Brief brief)
	{
		return "Sender:\n" + erstelleAdresskopf(brief.getSender());
	}
	
	protected String erstelleAdresskopf(Adresse adresse)
	{
		StringBuilder retval = new StringBuilder();

		retval.append(adresse.getVorname());
		retval.append(" ");	
		retval.append(adresse.getNachname());
		retval.append("\n");
		
		retval.append(adresse.getStrasse());
		retval.append(" ");	
		retval.append(adresse.getHausnummer());
		retval.append("\n");
		
		retval.append(adresse.getPlz());
		retval.append(" ");	
		retval.append(adresse.getOrt());
		retval.append("\n");
			
		return retval.toString();
	}
	
	protected String erstelleBriefinhalt(Inhalt inhalt)
	{
		StringBuilder retval = new StringBuilder();
		
		retval.append(inhalt.getTitel());
		retval.append("\n");
		retval.append(inhalt.getAnrede());
		retval.append("\n");
		retval.append(inhalt.getText());
		retval.append("\n");
		
		return retval.toString();
	}
}
