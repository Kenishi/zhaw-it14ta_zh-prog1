public class FensterBriefdruckStrategie extends BriefFormatterFactory
{
	private static final int INDENT = 10;
	
	@Override
	protected String formatiereEmpfaenger(Brief brief)
	{
		String empfaenger = "Empfänger:\n" + erstelleAdresskopf(brief.getEmpfaenger());
		return  empfaenger.replaceAll("(?m)^", convertIndentToSpace());
	}
	
	private String convertIndentToSpace()
	{
		StringBuilder retval = new StringBuilder();
		for(int space = 0; space < INDENT; space++)
		{
			retval.append(" ");
		}
		
		return retval.toString();
	}
	
}
